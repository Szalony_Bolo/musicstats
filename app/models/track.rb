class Track < ActiveRecord::Base
  belongs_to :album
  has_many :listens
  has_many :users, through: :listens
end