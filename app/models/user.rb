require 'rockstar'

class User < ActiveRecord::Base
  has_many :listens
  has_many :tracks, through: :listens
  #attr_accessor :username
  #validates_presence_of :username
end
