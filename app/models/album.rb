class Album < ActiveRecord::Base
  belong_to :artist
  has_many :tracks
end