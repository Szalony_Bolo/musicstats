# -*- encoding: utf-8 -*-
# stub: active_lastfm 1.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "active_lastfm"
  s.version = "1.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Ben Smith"]
  s.date = "2008-10-12"
  s.description = "Last.fm client for Ruby (and Rails) based on ActiveResource"
  s.email = "ben [at] thesmith [dot] co [dot] uk"
  s.extra_rdoc_files = ["README"]
  s.files = ["README"]
  s.homepage = "http://github.com/thesmith/active_lastfm"
  s.rdoc_options = ["--main", "README"]
  s.rubygems_version = "2.4.5.1"
  s.summary = "Last.fm client for Ruby (and Rails) based on ActiveResource"

  s.installed_by_version = "2.4.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 2

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activeresource>, [">= 2.0.2"])
    else
      s.add_dependency(%q<activeresource>, [">= 2.0.2"])
    end
  else
    s.add_dependency(%q<activeresource>, [">= 2.0.2"])
  end
end
