# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160803002454) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.integer  "artist_id"
    t.string   "title"
    t.date     "date"
    t.integer  "duration"
    t.string   "tags"
    t.integer  "plays"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "artists", force: :cascade do |t|
    t.string "artist"
  end

  create_table "listens", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "track_id"
    t.datetime "listen_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "listens", ["track_id"], name: "index_listens_on_track_id", using: :btree
  add_index "listens", ["user_id"], name: "index_listens_on_user_id", using: :btree

  create_table "songs", force: :cascade do |t|
    t.string "song"
  end

  create_table "tracks", force: :cascade do |t|
    t.integer  "album_id"
    t.string   "name"
    t.string   "tags"
    t.integer  "duration"
    t.integer  "plays"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
  end

end
