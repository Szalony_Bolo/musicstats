class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.belongs_to :album
      t.string :name
      t.string :tags
      t.integer :duration
      t.integer :plays

      t.timestamps
    end

  end
end
