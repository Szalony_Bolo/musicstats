class CreateListens < ActiveRecord::Migration
  def change
    create_table :listens do |t|
      t.integer :user_id
      t.integer :track_id
      t.datetime :listen_date
    end
    add_index :listens, :user_id
    add_index :listens, :track_id
  end
end
