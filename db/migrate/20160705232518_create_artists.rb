class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
    	t.string :name
      t.string :tags
      t.string :similar
      t.integer :listeners
      t.integer :plays

      t.timestamps
    end
  end
end
