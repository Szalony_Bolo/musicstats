class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.belongs_to :artist
      t.string :title
      t.date :date
      t.integer :duration
      t.string :tags
      t.integer :plays

      t.timestamps
    end
  end
end
